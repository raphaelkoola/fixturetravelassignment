# FixtureTravelAssignment
Using Twig template along with Symfony on Homestead to display an Image along with a countdown timer to an event.
- Symfony Project is located in Vagrant\~\Homestead\FixtureTravelAssignment.
- index.php renders the twig page which displays my age, my picture and the php file then displays the countdown timer to my next birthday.

##Tasks Completed:
- Set up Vagrant and Virtualbox on the local machine
- Set up a vagrant box (Laravel Homestead), installed and ran symfony. 
Note: Even though I was successful in setting up the vagrant box (Homestead) and running symfony server on it, I was unable to access any Symfony project on the virtualbox from my local machine.
- Comfigured Symfony app (FixtureTravelAssignment) to use Twig.
- Created a twig template (hello.twig) that displays my image and a countdown to my next birthday.

##Versions
- Vagrant: 2.2.4
- Virtualbox: 6.0.0
- Symfony: 4.5.3